from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder

import json

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.import_id}

@require_http_methods(["GET", "POST"])
def hat_list(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_id = content["location"]
            location = LocationVO.objects.get(import_id=location_id)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                    )

        hat = Hat.objects.create(**content)
        return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )

@require_http_methods(["GET", "DELETE"])
def hat_detail(request, pk):
    if request.method == 'GET':
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {'hat': 'hat'},
            encoder=HatDetailEncoder
        )

    else:
        count, _ = hat.objects.get(id=pk).delte()
        return JsonResponse({'deleted': count > 0})
