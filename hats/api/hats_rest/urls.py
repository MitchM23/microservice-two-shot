from django.urls import path
from . import views

urlpatterns = [
    path('hats/', views.hat_list, name='hat-list'),
    path('hats/<int:pk>/', views.hat_detail, name='hat-detail'),
]
