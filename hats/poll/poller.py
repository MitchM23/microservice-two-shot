import django
import os
import sys
import time
import json
import requests


sys.path.append(" ")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()
from hats_rest.models import Hat, LocationVO


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            # Write your polling logic here
            response = requests.get('http://wardrobe-api:8000/api/locations/')
            if response.status_code == 200:
                locations = response.json()

                for location in locations:
                    # Extract relevant information from the location data
                    import_id = location['import_id']
                    closet_name = location['closet_name']
                    section_number = location['section_number']
                    shelf_number = location['shelf_number']

                    # Create or update the LocationVO instance based on the retrieved data
                    location_obj, created = LocationVO.objects.get_or_create(
                        import_id=import_id,
                        closet_name=closet_name,
                        section_number=section_number,
                        shelf_number=shelf_number
                    )

                    # Extract hat information from location data
                    fabric = location['hat']['fabric']
                    style_name = location['hat']['style_name']
                    color = location['hat']['color']
                    picture_url = location['hat']['picture_url']

                    # Create or update the Hat instance based on the retrieved data and associated LocationVO
                    hat, created = Hat.objects.update_or_create(
                        fabric=fabric,
                        style_name=style_name,
                        color=color,
                        picture_url=picture_url,
                        location=location_obj
                    )

                    # Print the created/updated Hat instance
                    print(hat)

            else:
                print(f"Error fetching locations: {response.status_code}", file=sys.stderr)

        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)

if __name__ == "__main__":
    poll()
