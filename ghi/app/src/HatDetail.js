import React, { useEffect, useState } from 'react';

const HatDetail = ({ hatId }) => {
  const [hat, setHat] = useState(null);

  useEffect(() => {
    const fetchHat = async () => {
      try {
        const response = await fetch(`/api/hats/${hatId}/`);
        const data = await response.json();
        setHat(data);
      } catch (error) {
        console.error('Error fetching hat details:', error);
      }
    };

    fetchHat();
  }, [hatId]);

  if (!hat) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>{hat.style_name}</h1>
      <p>Fabric: {hat.fabric}</p>
      <p>Color: {hat.color}</p>
      <img src={hat.picture_url} alt="Hat" />
    </div>
  );
};

export default HatDetail;
