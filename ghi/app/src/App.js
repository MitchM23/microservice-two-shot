import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link, NavLink } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useState, useEffect } from 'react';
import HatDetail from './HatDetail';
import HatForm from './HatForm';
import ListHats from './ListHats';



import { useState, useEffect } from 'react';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App
