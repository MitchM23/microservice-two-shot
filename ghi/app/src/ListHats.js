import React, { useEffect, useState } from 'react';




function ListHats(props) {
  const deleteHat = async(hat) => {
    try{
      const deteled = hat.id
      const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`;
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(hatUrl, fetchConfig)
    if (response.ok) {
      props.getHats()
    };
  }
  catch (e) {
    console.log(e)
  }

  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th> Piture Url</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
        {props.hats.map(hat => {
          console.log(hat)
          return (
            <tr key={hat.id}>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td>{ hat.picture_url }</td>
              <td>{ hat.location}</td>
              <td><button onClick={() => deleteHat(hat)}>Delete</button></td>
            </tr>
          );
        })}
        </tbody>
      </table>
    );
}
}

export default ListHats;
