import React, { useState, useEffect } from 'react';

function HatForm ({ getHats }) {
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    async function fetchLocations() {
        const url = 'http://localhost:8100/api/locations'
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);

        }
      }

    fetchLocations();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      fabric,
      style_name: styleName,
      color,
      picture_url: pictureUrl,
      location,
    };


      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchConfig = {
        method: "post",
        body : JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
          const newHat = await response.json();
          console.log(newHat)
          getHats();

          setFabric('');
          setStyleName('');
          setColor('');
          setPictureUrl('');
          setLocation('');
      }
  }



  function handleFabricChange(event) {
    setFabric(event.target.value);
  }

  function handleStyleNameChange(event) {
    setStyleName(event.target.value);
  }

  function handleColorChange(event) {
    setColor(event.target.value);
  }

  function handlePictureUrlChange(event) {
    setPictureUrl(event.target.value);
  }

  function handleLocationChange(event) {
    setLocation(event.target.value);
  }

  return (
    <div>
      <h1>Create Hat</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Fabric:
          <input
            type="text"
            value={fabric}
            onChange={handleFabricChange}
          />
        </label>
        <label>
          Style Name:
          <input
            type="text"
            value={styleName}
            onChange={handleStyleNameChange}
          />
        </label>
        <label>
          Color:
          <input
            type="text"
            value={color}
            onChange={handleColorChange}
          />
        </label>
        <label>
          Picture URL:
          <input
            type="text"
            value={pictureUrl}
            onChange={handlePictureUrlChange}
          />
        </label>
        <label>
          Location:
          <select value={location} onChange={handleLocationChange}>
            <option value="">Select a location</option>
            {locations &&
              locations.map((location) => (
                <option key={location.id} value={location.id}>
                  {location.closet_name}
                </option>
              ))}
          </select>
        </label>
        <button type="submit">Create</button>
      </form>
    </div>
  );
}

export default HatForm;
