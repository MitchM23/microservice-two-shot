import React, { useState, useEffect } from 'react';

function HatForm(props) {
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [location, setLocation] = useState();
  const [locations, setLocations] = useState([]);


  useEffect(() => {
    async function fetchLocations() {
      const url = 'http://localhost:8100/api/wardrobe/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.location);
      }
    }
    fetchLocations();
  }, [])




  async function handleSubmit(event) {
    event.preventDefault();
    const newHat = {
      fabric,
      style_name: styleName,
      color,
      picture_url: pictureUrl,
      location,
    };

    try {
      const response = await fetch('/api/hats/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newHat),
      });

      if (response.ok) {
        // Hat created successfully, perform desired action (e.g., redirect)
      } else {
        console.error('Error creating hat:', response.status);
      }
    } catch (error) {
      console.error('Error creating hat:', error);
    }
  }

  return (
    <div>
      <h1>Create Hat</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Fabric:
          <input
            type="text"
            value={fabric}
            onChange={(event) => setFabric(event.target.value)}
          />
        </label>
        <label>
          Style Name:
          <input
            type="text"
            value={styleName}
            onChange={(event) => setStyleName(event.target.value)}
          />
        </label>
        <label>
          Color:
          <input
            type="text"
            value={color}
            onChange={(event) => setColor(event.target.value)}
          />
        </label>
        <label>
          Picture URL:
          <input
            type="text"
            value={pictureUrl}
            onChange={(event) => setPictureUrl(event.target.value)}
          />
        </label>
        <button type="submit">Create</button>
      </form>
    </div>
  );
}

export default HatForm;
