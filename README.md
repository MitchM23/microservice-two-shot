# Wardrobify

Team:

* Rosanna Wyatt -Shoes
* Mitchell Mora - Hats

## Design

## Shoes microservice

I will create a model for Shoes and a model for BinVO (bin value objects), and set up a poller to get data from the wardrobe microservice to update The binVO data. This will allow the shoes api to know what bins are available so a bin value can be assigned for my shoes that will correspond to an existing bin in the wardrobe.

## Hats microservice

I made a hat detail model, location model/enconder. The hat detail model passes information to the warddrobe api to render a list of hats, create hats after the form is filled out. And the location encoder has locations in the drop down menu of my ats form as it communicates with the wardrobe api.
